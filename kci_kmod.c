#undef __KERNEL__
#define __KERNEL__
#undef MODULE
#define MODULE

#include "kci.h"

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/string.h>
#include <linux/init.h>
#include <linux/debugfs.h>
#include <linux/delay.h>
#include <asm/paravirt.h>
#include <linux/sched.h>
#include <linux/syscalls.h>

#define PRTK_ERROR(MSG) printk(KERN_ALERT "%s\n", MSG)

#define BUF_SIZE (PAGE_SIZE << 2)
#define LOG_SIZE 256

#define READ_REQ_BUF "=== New read request ===\nProcess ID: %d\nFile descriptor: %d\nRequested bytes: %zu\n"
#define WRITE_REQ_BUF "=== New write request ===\nProcess ID: %d\nFile descriptor: %d\nRequested bytes: %zu\n"
#define RES_BUF "Successful bytes: %zu\n"

MODULE_LICENSE("GPL");

/* * * * * * * GLOBAL VARIABLES * * * * * * */
static pid_t _pid; 
static int _fd;
static int _cipher;

static unsigned long** _sct;
static unsigned long _cr0;

static struct dentry* _file;
static struct dentry* _subdir;

static char _glob_buffer[BUF_SIZE] = {0};
static size_t _buffer_pos = 0;

asmlinkage long (* _read)(int fd, char* __user buf, size_t count);
asmlinkage long (* _write)(int fd, char* __user buf, size_t count);

/* * * * * * * WRITING TO GLOBAL BUFFER * * * * * * */
void writeToGlobBuffer(char* buf)
{
	while(*buf)
	{
		// Avoid overflow
		if(_buffer_pos >= BUF_SIZE)
		{
			memset(_glob_buffer, 0, BUF_SIZE);
			_buffer_pos = 0;
		}

		_glob_buffer[_buffer_pos] = *buf;

		buf++;
		_buffer_pos++;
	}
}

/* * * * * * * READ/WRITE OVERRIDE * * * * * * */
asmlinkage long kciRead(int fd, char* __user buf, size_t count)
{
	size_t i, bytes_read;
	char curr_byte;
	int rc;
	char request_log[LOG_SIZE], result_log[LOG_SIZE];

	// First, use actual read
	bytes_read = _read(fd, buf, count);

	// If (PID,ID) don't match, quit
	if(current->pid != _pid || _fd != fd) return bytes_read;

	// If (PID,FD) match, use overridden read
	sprintf(request_log, READ_REQ_BUF, _pid, _fd, count);
	pr_debug(READ_REQ_BUF, _pid, _fd, count);
	writeToGlobBuffer(request_log);

	// Decrypt buffer
	for(i = 0; i < bytes_read; i++)
	{
		rc = get_user(curr_byte, buf + i);
		if(rc < 0)
		{
			PRTK_ERROR("get_user() failed");
			return rc;
		}

		curr_byte -= _cipher;

		rc = put_user(curr_byte, buf + i);
		if(rc < 0)
		{
			PRTK_ERROR("put_user() failed");
			return rc;
		}
	}

	// Log operation result
	sprintf(result_log, RES_BUF, bytes_read);
	pr_debug(RES_BUF, bytes_read);
	writeToGlobBuffer(result_log);

	return bytes_read;
}

asmlinkage long kciWrite(int fd, char* __user buf, size_t count)
{
	size_t bytes_written, i;
	char curr_byte;
	int rc;
	char request_log[LOG_SIZE], result_log[LOG_SIZE];

	// If (PID,FD) don't match, use actual write
	if(current->pid != _pid || _fd != fd) return _write(fd, buf, count);

	// If (PID,FD) match, use overridden write
	sprintf(request_log, WRITE_REQ_BUF, _pid, _fd, count);
	pr_debug(WRITE_REQ_BUF, _pid, _fd, count);
	writeToGlobBuffer(request_log);

	// Encrypt buffer
	for(i = 0; i < count; i++)
	{
		rc = get_user(curr_byte, buf + i);
		if(rc < 0)
		{
			PRTK_ERROR("get_user() failed");
			return rc;
		}

		curr_byte += _cipher;

		rc = put_user(curr_byte, buf + i);
		if(rc < 0)
		{
			PRTK_ERROR("put_user() failed");
			return rc;
		}
	}

	// Perform actual write on encrypted buffer
	bytes_written = _write(fd, buf, count);

	// Decrypt buffer back to original state
	for(i = 0; i < count; i++)
	{
		rc = get_user(curr_byte, buf + i);
		if(rc < 0)
		{
			PRTK_ERROR("get_user() failed");
			return rc;
		}

		curr_byte -= _cipher;

		rc = put_user(curr_byte, buf + i);
		if(rc < 0)
		{
			PRTK_ERROR("put_user() failed");
			return rc;
		}
	}

	// Log operation result
	sprintf(result_log, RES_BUF, bytes_written);
	pr_debug(RES_BUF, bytes_written);
	writeToGlobBuffer(result_log);

	return bytes_written;
}

static ssize_t dbgRead(struct file* filp, char* buffer, size_t len, loff_t* offset)
{
	return simple_read_from_buffer(buffer, len, offset, _glob_buffer, _buffer_pos);
}

/* * * * * * * IOCTL * * * * * * */
static long kciIoctl(struct file* file, unsigned int ioctl_num, unsigned long ioctl_param) 
{
	switch(ioctl_num)
	{
		case IOCTL_SET_PID:
			_pid = ioctl_param;
			// printk("PID set to %lu.\n", ioctl_param);
			break;

		case IOCTL_SET_FD:
			_fd = ioctl_param;
			// printk("FD set to %lu.\n", ioctl_param);
			break;

		case IOCTL_CIPHER:
			_cipher = ioctl_param;
			// printk("Cipher flag set to %lu.\n", ioctl_param);
			break;
	}

	return SUCCESS;
}

/* * * * * * * FILE OPERATIONS * * * * * * */
struct file_operations kci_ops = {
	.owner = THIS_MODULE,
    //.read = device_read,
    //.write = device_write,
    .unlocked_ioctl= kciIoctl,
    //.open = device_open,
    //.release = device_release,  
};

// TODO: is it correct?
struct file_operations debug_ops = {
	.owner = THIS_MODULE,
    .read = dbgRead,
    // .write = kciWrite,
    // .unlocked_ioctl= device_ioctl,
    //.open = device_open,
    //.release = device_release,  
};

static unsigned long** acquireSCT(void)
{
	unsigned long int offset = PAGE_OFFSET;
	unsigned long** sct;

	while(offset < ULLONG_MAX)
	{
		sct = (unsigned long**) offset;
		if(sct[__NR_close] == (unsigned long*) sys_close) return sct;
		offset += sizeof(void*);
	}

	return NULL;
}

static int intercept(void) 
{
	// Acquire table
	if(!(_sct = acquireSCT())) return -1;

	// Manipulate table
	_cr0 = read_cr0();
	write_cr0(_cr0 & ~0x00010000);
	_read = (void*) _sct[__NR_read];
	_write = (void*) _sct[__NR_write];
	_sct[__NR_read] = (unsigned long*) kciRead;
	_sct[__NR_write] = (unsigned long*) kciWrite;
	write_cr0(_cr0);

	return 0;
}

static void unintercept(void) 
{
	if(!_sct) return;

	// Restore table
	write_cr0(_cr0 & ~0x00010000);
	_sct[__NR_read] = (unsigned long*) _read;
	_sct[__NR_write] = (unsigned long*) _write;
	write_cr0(_cr0);

	msleep(2000);
}

static int __init kciInit(void)
{
	unsigned int rc = 0; 

	// Register chrdev
	rc = register_chrdev(MAJOR_NUM, DEVICE_RANGE_NAME, &kci_ops);
	if(rc < 0)
	{
		PRTK_ERROR("Registering failed");
		return -1;
	}

	// Create debug folder and file
	_subdir = debugfs_create_dir("kcikmod", NULL);
	if(IS_ERR(_subdir)) return PTR_ERR(_subdir);
	if(!_subdir) return -ENOENT;

	_file = debugfs_create_file("calls", S_IRUSR, _subdir, NULL, &debug_ops);
	if(!_file)
	{
		debugfs_remove_recursive(_subdir);
		return -ENOENT;
	}

	// Initialize global variables
	_pid = -1;
	_fd = -1;
	_cipher= 0;
	_buffer_pos = 0;

	// Intercept read/write
	rc = intercept();
	if(rc < 0)
	{
		PRTK_ERROR("Read/Write interception failed");
		return -1;
	}

	return 0;
}

static void __exit kciExit(void)
{
	unintercept();
	unregister_chrdev(MAJOR_NUM, DEVICE_RANGE_NAME);
	debugfs_remove_recursive(_subdir);
}

module_init(kciInit);
module_exit(kciExit);