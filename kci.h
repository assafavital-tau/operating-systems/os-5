#ifndef KCI_H
#define KCI_H

#include <linux/ioctl.h>

#define PRIVATE_LOG_FILE "/sys/kernel/debug/kcikmod/calls"
#define DEVICE_FILE_NAME "/dev/kci_dev"
#define DEVICE_RANGE_NAME "kci_kmod"

#define MAJOR_NUM 245
#define MINOR_NUM 0

#define SUCCESS 0

/* * * * * * * IOCTL COMMANDS * * * * * * */
#define IOCTL_SET_PID _IOW(MAJOR_NUM, 0, int)
#define IOCTL_SET_FD _IOW(MAJOR_NUM, 1, int)
#define IOCTL_CIPHER _IOW(MAJOR_NUM, 2, int)

#endif