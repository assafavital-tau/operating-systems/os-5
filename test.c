#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>

#include <errno.h>
#include <string.h>

#define FILE_NAME "in_out.txt"
#define BUF_SIZE 1024

#define QUIT_STR "quit"

int main(int argc, const char *argv[]) {
	int fd = open(FILE_NAME, O_CREAT | O_RDWR | O_TRUNC, 0777);
	if (fd < 0) {
		printf("Error: Create file failed: %s\n", strerror(errno));
		return errno;
	}
	printf("Created %s. file descriptor is %d\n", FILE_NAME, fd);

	char buf[BUF_SIZE];
	while(1) {
		printf("Write a string with no spaces to write to the file ('%s' to terminate):\n", QUIT_STR);
		if (scanf("%s", buf) < 0) {
			printf("Error: Reading from stdin failed.\n");
			close(fd);
			return -1;
		}

		if (!strcmp(buf, QUIT_STR))
			break;

		if (write(fd, buf, strlen(buf)) < strlen(buf)) {
			printf("Error: Write to file failed. %s\n", strerror(errno));
			close(fd);
			return errno;
		}
	}

	if (lseek(fd, 0, SEEK_SET) < 0) {
		printf("Error: Seek to start of file faild. %s\n", strerror(errno));
		return errno;
	}

	int read_bytes;
	printf("Content of file:\n");
	buf[BUF_SIZE-1] = '\0';
	while ((read_bytes = read(fd, buf, BUF_SIZE-1)) > 0) {
		printf("%s", buf);
	}
	if (read_bytes < 0) {
		printf("Error: Read from file failed. %s\n", strerror(errno));
		close(fd);
		return errno;
	}

	close(fd);
	return 0;
}