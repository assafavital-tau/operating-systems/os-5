#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/sysmacros.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

#include "kci.h"

#define PRT_ERR(MSG,ERR) printf("[ERROR] %s: %s.\n", MSG, strerror(ERR))

#define LOG_CALLS "calls"

#define CMD_INIT "-init"
#define CMD_PID "-pid"
#define CMD_FD "-fd"
#define CMD_START "-start"
#define CMD_STOP "-stop"
#define CMD_RM "-rm"

#define BUFFER_LIMIT 4096

/* * * * * * * INIT * * * * * * */
void kciInit(const char* ko_path)
{
	// Open KO file
	int ko_fd = open(ko_path, O_RDONLY);
	if(ko_fd < 0)
	{
		PRT_ERR("Could not open KO file", errno);
		exit(errno);
	}

	// Call finit_module
	int rc = syscall(__NR_finit_module, ko_fd, "", 0);
	if(rc < 0)
	{
		PRT_ERR("finit_module() failed", errno);
		exit(errno);
	}

	// Create dev_t
	dev_t id = makedev(MAJOR_NUM, MINOR_NUM);

	// Create dev node
	rc = mknod(DEVICE_FILE_NAME, S_IFCHR, id);
	if(rc < 0)
	{
		PRT_ERR("mknod() failed", errno);
		exit(errno);
	}

	close(ko_fd);
}

/* * * * * * * PID * * * * * * */
void kciSetPID(int pid)
{
	// Open dev node
	int dev_fd = open(DEVICE_FILE_NAME, 0);
	if(dev_fd < 0)
	{
		PRT_ERR("Could not open dev file", errno);
		exit(errno);
	}

	// Set PID
	int ret_val = ioctl(dev_fd, IOCTL_SET_PID, pid);
	if(ret_val < 0)
	{
		printf("Failed to set PID.\nExiting...\n");
		exit(ret_val);
	}
}

/* * * * * * * FD * * * * * * */
void kciSetFD(int fd)
{
	// Open dev node
	int dev_fd = open(DEVICE_FILE_NAME, 0);
	if(dev_fd < 0)
	{
		PRT_ERR("Could not open dev file", errno);
		exit(errno);
	}

	// Set FD
	int ret_val = ioctl(dev_fd, IOCTL_SET_FD, fd);
	if(ret_val < 0)
	{
		printf("Failed to set FD.\nExiting...\n");
		exit(ret_val);
	}
}

/* * * * * * * START * * * * * * */
void kciStart()
{
	// Open dev node
	int dev_fd = open(DEVICE_FILE_NAME, 0);
	if(dev_fd < 0)
	{
		PRT_ERR("Could not open dev file", errno);
		exit(errno);
	}

	// Set cipher to 1
	int ret_val = ioctl(dev_fd, IOCTL_CIPHER, 1);
	if(ret_val < 0)
	{
		printf("Failed to set cipher flag.\nExiting...\n");
		exit(ret_val);
	}
}

/* * * * * * * STOP * * * * * * */
void kciStop()
{
	// Open dev node
	int dev_fd = open(DEVICE_FILE_NAME, 0);
	if(dev_fd < 0)
	{
		PRT_ERR("Could not open dev file", errno);
		exit(errno);
	}

	// Set cipher to 0
	int ret_val = ioctl(dev_fd, IOCTL_CIPHER, 0);
	if(ret_val < 0)
	{
		printf("Failed to set cipher flag.\nExiting...\n");
		exit(ret_val);
	}
}

/* * * * * * * RM * * * * * * */
void kciRemove()
{
	// TODO: can't read log file
	// Open private log file
	int plf_fd = open(PRIVATE_LOG_FILE, O_RDONLY);
	if(plf_fd < 0)
	{
		PRT_ERR("Could not open private log file", errno);
		exit(errno);
	}

	// Open output file
	int calls_fd = open(LOG_CALLS, O_WRONLY | O_CREAT | O_TRUNC, 0777);
	if(calls_fd < 0)
	{
		PRT_ERR("Could not open 'calls'", errno);
		exit(errno);
	}

	// Copy contents
	char buff[BUFFER_LIMIT];
	ssize_t bytes_read, bytes_written, tmp_write;
	do
	{
		// Read from input
		bytes_read = read(plf_fd, buff, BUFFER_LIMIT);
		if(bytes_read == 0) break;
		if(bytes_read < 0)
		{
			PRT_ERR("Could not read from private log file", errno);
			exit(errno);
		}

		// Write to output
		bytes_written = 0;
		while(bytes_written < bytes_read)
		{
			tmp_write = write(calls_fd, buff + bytes_written, bytes_read - bytes_written);
			if(tmp_write < 0)
			{
				PRT_ERR("Could not write to calls file", errno);
				exit(errno);
			}

			bytes_written += tmp_write;
		}
	}
	while(1);

	// Close resources
	if(close(plf_fd) < 0)
	{
		PRT_ERR("Could not close private log file", errno);
		exit(errno);
	}
	if(close(calls_fd) < 0)
	{
		PRT_ERR("Could not close calls file", errno);
		exit(errno);
	}

	// Delete module
	int rc = syscall(__NR_delete_module, DEVICE_RANGE_NAME, O_NONBLOCK);
	if(rc < 0)
	{
		PRT_ERR("delete_module() failed", errno);
		exit(errno);
	}

	// Remove dev file
	rc = unlink(DEVICE_FILE_NAME);
	if(rc < 0)
	{
		PRT_ERR("Could not remove dev file", errno);
		exit(errno);
	}
}

int main(int argc, char** argv)
{
	/* * * * * * * COMMAND-LINE ARGUMENTS * * * * * * */
	if(argc < 2)
	{
		printf("Invalid command-line arguments.\nExiting...\n");
		exit(-1);
	}

	char* arg = argv[1];

	/* * * * * * * CONTROL UNIT * * * * * * */
	if(strcmp(arg, CMD_INIT) == 0) {kciInit(argv[2]); exit(0);}
	if(strcmp(arg, CMD_PID) == 0) {kciSetPID(atoi(argv[2])); exit(0);}
	if(strcmp(arg, CMD_FD) == 0) {kciSetFD(atoi(argv[2])); exit(0);}
	if(strcmp(arg, CMD_START) == 0) {kciStart(); exit(0);}
	if(strcmp(arg, CMD_STOP) == 0) {kciStop(); exit(0);}
	if(strcmp(arg, CMD_RM) == 0) {kciRemove(); exit(0);}

	printf("Invalid command-line arguments.\nExiting...\n");
	exit(-1);
}